# fizz_buzz.py
# Programmer: Demar Brown (Demar Codes)
# Date: Jan 1, 2021
# Program Details: This program prints the numbers 1-100.
                    #For multiples of 3, "Fizz" is printed instead of the number.
                    #For multiples of 5, "Buzz" is printed instead of the number.
                    #For multiples of 3, "FizzBuzz" is printed instead of the number.

#-------------------
# ------------------------------

#Program begins

print("\nThis is a fizzbuzz program with range 1 - 100.\n")

for num in range(1,101):

    if num%3 == 0:
        if num%5 == 0:
            print("FizzBuzz")
        else:
            print("Fizz")
    elif num % 5 == 0:
        print("Buzz")
    else:
        print(num)